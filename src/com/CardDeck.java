package com;

import static java.lang.Math.random;

/**
 * Created by Евгений on 18.07.2017.
 */
class CardDeck <T>{

    private T[] cards;
    private int cardDeck = AllNumberOfInGame.numberOfCards;

    CardDeck(){
        cards = (T[])  new Card[AllNumberOfInGame.numberOfCards];
        this.initCardDeck();
    }
    private  void initCardDeck(){
        int n = 0;
        for (int i = 0; i < CardValues.values().length; i++) {
            for (int j = 0; j < CardSuites.values().length; j++) {
                this.cards[n] = (T) new Card(CardSuites.values()[j], CardValues.values()[i]);
                n++;
            }
        }
    }


    void search(){
        T cards;
        int index;
        for(int i = 0; i < this.cards.length; i++){
            index = (int) (random() * (this.cards.length - i)) + i;
            cards = this.cards[i];
            this.cards[i] = this.cards[index];
            this.cards[index] = cards;

        }
    }
    private boolean cashCards() {
        return this.cardDeck > 0;
    }

    T drawOne() {
        if (cashCards()) {
            T cardD = this.cards[0];
            T[] newCard = (T[]) new Card[this.cardDeck - 1];
            for (int i = 0; i < this.cardDeck - 1; i++) {
                newCard[i] = this.cards[i + 1];
            }
            this.cards = (T[]) new Card[this.cardDeck - 1];
            this.cards = newCard;

            this.cardDeck--;

            return cardD;
        } else {
            return null;
        }
    }
}





