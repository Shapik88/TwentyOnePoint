package com;

/**
 * Created by Евгений on 18.07.2017.
 */
public enum CardSuites {
    HEARTS,
    SPADES,
    DIAMONDS,
    CLUBS
}
