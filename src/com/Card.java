package com;



/**
 * Created by Евгений on 18.07.2017.
 */
public class Card {

    private final CardSuites cardSuites;
    private final  CardValues cardValues;

    Card(CardSuites cardSuites, CardValues cardValues){
        this.cardSuites = cardSuites;
        this.cardValues = cardValues;
    }

    public CardSuites getCardSuites() {

        return cardSuites;
    }

    public CardValues getCardValues() {

        return cardValues;
    }
    public int getValue() {
        switch (this.getCardValues()){

            case ACE: return 1;
            case TWO: return 2;
            case THREE: return 3;
            case FOUR: return 4;
            case FIVE: return 5;
            case SIX: return 6;
            case SEVEN: return 7;
            case EIGHT: return 8;
            case NINE: return 9;
            case TEN: return 10;
            case JACK: return 11;
            case QUIN: return 12;
            case KING: return 13;
            default: return 0;
        }
    }

}
